$(document).ready(function() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -53.786296, lng: -67.696483 },
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker = new google.maps.Marker({
        position: { lat: -53.786296, lng: -67.696483 },
        map: map
    });
})